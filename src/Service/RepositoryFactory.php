<?php
/**
 * User: krona
 * Date: 9/29/14
 * Time: 6:18 PM
 */

namespace Krona\MongoODM\Service;

use Krona\MongoODM\DocumentManager;
use Krona\MongoODM\Exception\RuntimeException;
use Krona\MongoODM\Repository\DocumentRepository;

class RepositoryFactory
{
    /** @var  DocumentRepository[] */
    protected $instances = [];
    /** @var  DocumentManager */
    protected $documentManager;

    public function __construct(DocumentManager $documentManager)
    {
        $this->documentManager = $documentManager;
    }

    /**
     * @param string $className
     * @param string $identifier
     * @return DocumentRepository
     */
    public function getRepository($className, $identifier = '')
    {
        if (!isset($this->instances[$className][$identifier])) {
            $this->createInstance($className, $identifier);
        }

        return $this->instances[$className][$identifier];
    }

    /**
     * @param string $className
     * @param string $identifier
     */
    protected function createInstance($className, $identifier = '')
    {
        if (class_exists($className)) {
            $metadata = $this->documentManager->getClassMetadata($className);
            $repositoryClassName = $metadata->customRepositoryClass;
            $collectionConfig = $metadata->collection;
            $collection = $this->documentManager->getDatabase()->createCollection(
                $collectionConfig['name'] . (($identifier != '') ? '_' . $identifier : ''),
                $collectionConfig['capped'],
                $collectionConfig['size']
            );
            if (is_null($repositoryClassName)) {
                $repository = new DocumentRepository($this->documentManager, $metadata, $collection);
            } else {
                $repository = new $metadata->customRepositoryClass($this->documentManager, $metadata, $collection);
            }

            $this->instances[$className][$identifier] = $repository;
        } else {
            throw new RuntimeException(
                'Document with class Name:' . $className . ' not found, check your Configuration'
            );
        }
    }

    public function clearObjects()
    {
        foreach ($this->instances as $repository) {
            $repository->getUnitOfWork()->clear();
        }
    }
} 