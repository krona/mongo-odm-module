<?php
/**
 * User: krona
 * Date: 9/29/14
 * Time: 6:22 PM
 */

namespace Krona\MongoODM\Service;


use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\FileCacheReader;
use Doctrine\Common\Persistence\Mapping\ClassMetadataFactory;
use Krona\MongoODM\Mapping\ClassMetadata;
use Krona\MongoODM\Mapping\Driver\AnnotationDriver;

class MetadataFactory implements ClassMetadataFactory
{
    protected $reader;
    protected $annotationDriver;
    protected $loadedMetadata;

    public function __construct(array $config)
    {
        $this->reader = new FileCacheReader(
            new AnnotationReader(),
            $config['mongo']['configuration']['orm_default']['cache_dir'],
            $config['mongo']['configuration']['orm_default']['debug']
        );
        $this->annotationDriver = new AnnotationDriver($this->reader);
    }

    /**
     * Forces the factory to load the metadata of all classes known to the underlying
     * mapping driver.
     *
     * @return array The ClassMetadata instances of all mapped classes.
     */
    public function getAllMetadata()
    {
        return $this->loadedMetadata;
    }

    /**
     * Gets the class metadata descriptor for a class.
     *
     * @param string $className The name of the class.
     *
     * @return ClassMetadata
     */
    public function getMetadataFor($className)
    {
        if ($this->hasMetadataFor($className)) {
            return $this->loadedMetadata[$className];
        }

        $this->loadMetadata($className);

        return $this->loadedMetadata[$className];
    }

    /**
     * Checks whether the factory has the metadata for a class loaded already.
     *
     * @param string $className
     *
     * @return boolean TRUE if the metadata of the class in question is already loaded, FALSE otherwise.
     */
    public function hasMetadataFor($className)
    {
        return isset($this->loadedMetadata[$className]);
    }

    protected function loadMetadata($entityName)
    {
        $metadata = $this->createClassMetadataFor($entityName);
        $this->annotationDriver->loadMetadataForClass($entityName, $metadata);
        $this->setMetadataFor($entityName, $metadata);
    }

    protected function createClassMetadataFor($entityName)
    {
        $metadata = new ClassMetadata($entityName);
        $metadata->reflClass = new \ReflectionClass($entityName);
        $metadata->reflFields = $metadata->reflClass->getProperties();

        return $metadata;
    }

    /**
     * Sets the metadata descriptor for a specific class.
     *
     * @param string        $className
     *
     * @param ClassMetadata $class
     */
    public function setMetadataFor($className, $class)
    {
        $this->loadedMetadata[$className] = $class;
    }

    /**
     * Returns whether the class with the specified name should have its metadata loaded.
     * This is only the case if it is either mapped directly or as a MappedSuperclass.
     *
     * @param string $className
     *
     * @return boolean
     */
    public function isTransient($className)
    {
        return true;
    }

    /**
     * @return AnnotationReader
     */
    public function getReader()
    {
        return $this->reader;
    }
}