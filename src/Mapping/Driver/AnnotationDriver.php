<?php
/**
 * User: krona
 * Date: 9/30/14
 * Time: 3:36 PM
 */

namespace Krona\MongoODM\Mapping\Driver;

use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Doctrine\Common\Persistence\Mapping\Driver\AnnotationDriver as AbstractAnnotationDriver;
use Krona\MongoODM\Document\TypedDocumentInterface;
use Krona\MongoODM\Mapping\ClassMetadata as KronaClassMetadata;
use Krona\MongoODM\Mapping\Collection;
use Krona\MongoODM\Mapping\Document;
use Krona\MongoODM\Mapping\Field;
use Krona\MongoODM\Mapping\Id;

class AnnotationDriver extends AbstractAnnotationDriver
{
    protected $abbrs = [
        'date' => 'mongoDate',
        'datetime' => 'mongoDate',
    ];

    /**
     * Loads the metadata for the specified class into the provided container.
     *
     * @param string                           $className
     * @param ClassMetadata|KronaClassMetadata $metadata
     *
     * @return void
     */
    public function loadMetadataForClass($className, ClassMetadata $metadata)
    {
        $class = $metadata->getReflectionClass();
        if (!$class) {
            $class = new \ReflectionClass($metadata->getName());
            $metadata->reflClass = $class;
        }

        $classAnnotations = $this->reader->getClassAnnotations($class);

        if ($classAnnotations) {
            foreach ($classAnnotations as $key => $annotation) {
                if (!is_numeric($key)) {
                    continue;
                }
                $classAnnotations[get_class($annotation)] = $annotation;
            }
        }

        if (isset($classAnnotations[Document::class])) {
            /** @var Document $documentAnnotation */
            $documentAnnotation = $classAnnotations[Document::class];
            if (!is_null($documentAnnotation->repositoryClass)) {
                $metadata->setCustomRepositoryClass($documentAnnotation->repositoryClass);
            }
        }
        if (isset($classAnnotations[Collection::class])) {
            /** @var Collection $collectionAnnotation */
            $collectionAnnotation = $classAnnotations[Collection::class];
            $collection = [
                'name' => $collectionAnnotation->name,
                'capped' => $collectionAnnotation->capped,
                'size' => $collectionAnnotation->size,
            ];
            if($class->implementsInterface(TypedDocumentInterface::class)) {
                $collection['typed'] = true;
                $collection['type'] = $class->getName();
            }
            $metadata->setCollection($collection);
        }

        foreach ($class->getProperties() as $property) {
            $mapping = [
                'fieldName' => $property->getName(),
            ];

            /** @var Field $fieldAnnotation */
            if ($fieldAnnotation = $this->reader->getPropertyAnnotation($property, Field::class)) {
                $mapping['type'] = (isset($this->abbrs[$fieldAnnotation->type])) ? $this->abbrs[$fieldAnnotation->type] : $fieldAnnotation->type;
                $mapping['name'] = (!is_null($fieldAnnotation->name)) ? $fieldAnnotation->name : $property->getName();

                /** @var Id $idAnnotation */
            } elseif ($idAnnotation = $this->reader->getPropertyAnnotation($property, Id::class)) {
                $mapping['id'] = true;
                $mapping['type'] = 'id';
                $metadata->setIdentifier($property->getName());
                $mapping['name'] = '_id';
            }

            $metadata->fieldMappings[$property->getName()] = $mapping;
            $metadata->fieldNames[$property->getName()] = $mapping;
        }
    }
}