<?php
/**
 * User: krona
 * Date: 9/30/14
 * Time: 3:55 PM
 */

namespace Krona\MongoODM\Mapping;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * Class Id
 * @package Krona\MongoODM\Mapping
 * @Annotation
 * @Target({"PROPERTY"})
 */
class Id
{

} 