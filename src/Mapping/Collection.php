<?php
/**
 * User: krona
 * Date: 9/29/14
 * Time: 6:54 PM
 */

namespace Krona\MongoODM\Mapping;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * Class Collection
 * @package Krona\MongoODM\Mapping
 * @Annotation
 * @Target({"CLASS"})
 */
class Collection
{
    public $name;

    public $capped = false;

    public $size = 0;
} 