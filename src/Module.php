<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 9/25/14
 * Time: 6:39 PM
 */

namespace Krona\MongoODM;


class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/Resources/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__,
                ),
            ),
        );
    }
} 