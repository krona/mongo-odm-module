<?php
/**
 * User: krona
 * Date: 10/6/14
 * Time: 3:22 PM
 */

namespace Krona\MongoODM\Common;


use Krona\Common\Common\Converter\ConverterInterface;
use Krona\MongoODM\Exception\RuntimeException;

class DateTimeConverter implements ConverterInterface
{
    const SQL_DATETIME = 'Y-m-d H:i:s';

    /**
     * Convert to PHP type
     * @param $value
     * @return mixed
     */
    public function convert($value)
    {
        if ($value instanceof \MongoDate) {
            return \DateTime::createFromFormat('U', $value->sec);
        } elseif (is_string($value) && $value != '') {
            return new \DateTime($value);
        } elseif (is_array($value)) {
            if (isset($value['sec'])) {
                return \DateTime::createFromFormat('U', $value['sec']);
            } else {
                throw new RuntimeException(
                    'Unknown type of Date'
                );
            }
        } else {
            return null;
        }
    }

    /**
     * Convert to SQL type
     * @param      $value
     * @param bool $asColumn
     * @return mixed
     */
    public function revert($value, $asColumn = true)
    {
        if (is_null($value)) {
            return $value;
        }
        if ($asColumn) {
            if ($value instanceof \DateTime) {
                return new \MongoDate($value->format('U'));
            } elseif (is_string($value)) {
                return new \MongoDate(strtotime($value));
            }
        } else {
            if ($value instanceof \DateTime) {
                return $value->format(static::SQL_DATETIME);
            } else {
                return $value;
            }
        }

        return null;
    }
}