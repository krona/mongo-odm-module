<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 9/29/14
 * Time: 4:43 PM
 */

namespace Krona\MongoODM\Repository;


use Doctrine\MongoDB\Collection;
use Krona\Common\Common\UnitOfWork;
use Krona\Common\Object\ObjectInterface;
use Krona\Common\Repository\ObjectRepository;
use Krona\MongoODM\Document\DocumentInterface;
use Krona\MongoODM\Document\TypedDocumentInterface;
use Krona\MongoODM\DocumentManager;
use Krona\MongoODM\Mapping\ClassMetadata;

class DocumentRepository implements ObjectRepository
{
    /** @var ClassMetadata */
    protected $metadata;
    /** @var  DocumentManager */
    protected $documentManager;
    /** @var Collection */
    protected $collection;
    /** @var  UnitOfWork */
    protected $unitOfWork;

    public function __construct(DocumentManager $documentManager, ClassMetadata $metadata, Collection $collection)
    {
        $this->documentManager = $documentManager;
        $this->metadata = $metadata;
        $this->collection = $collection;
        $this->unitOfWork = new UnitOfWork($this);
    }

    /**
     * @param ObjectInterface|DocumentInterface $document
     * @return bool
     */
    public function commit(ObjectInterface $document)
    {
        $data = $this->getUnitOfWork()->calculateChanges($document);

        if (!empty($data)) {
            $data = $this->documentManager->getObjectHydrator()->extract($document);
            if($document instanceof TypedDocumentInterface){
                $data['__document_class'] = get_class($document);
            }
            $result = $this->collection->save($data);
            if ($result['ok']) {
                if (is_null($document->getId())) {
                    $document->setId((string)$result['upserted']);
                }
                $this->unitOfWork->updateOrigin($document);
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * @return UnitOfWork
     */
    public function getUnitOfWork()
    {
        return $this->unitOfWork;
    }

    /**
     * @param UnitOfWork $unitOfWork
     * @return $this
     */
    public function setUnitOfWork($unitOfWork)
    {
        $this->unitOfWork = $unitOfWork;
        return $this;
    }

    /**
     * Method returns Metadata of managed Object
     * @return ClassMetadata
     */
    public function getClassMetadata()
    {
        return $this->metadata;
    }

    /**
     * Finds an object by its primary key / identifier.
     *
     * @param mixed $id The identifier.
     *
     * @return DocumentInterface The object.
     */
    public function find($id)
    {
        try {
            if(isset($this->metadata->collection['typed'])) {
                if($this->metadata->collection['typed']) {
                    $row = $this->collection->findOne(
                        [
                            '_id' => new \MongoId($id),
                            '__document_class' => $this->metadata->collection['type']
                        ]
                    );
                }
            } else {
                $row = $this->collection->findOne(
                    [
                        '_id' => new \MongoId($id)
                    ]
                );
            }

        } catch (\MongoException $e) {
            return null;
        }

        if ($row) {
            return $this->unitOfWork->hydrate($row);
        } else {
            return null;
        }
    }

    /**
     * Finds all objects in the repository.
     *
     * @return DocumentInterface[].
     */
    public function findAll()
    {
        $documents = [];

        if(isset($this->metadata->collection['typed'])) {
            if($this->metadata->collection['typed']) {
                $result = $this->getCollection()->aggregate([
                    '$match' => [
                        '__document_class' => isset($this->metadata->collection['type'])? $this->metadata->collection['type'] : '',
                    ],
                ]);
            } else {
                $result = $this->getCollection()->find();
            }
        } else {
            $result = $this->getCollection()->find();
        }

        foreach ($result as $rawDocument) {
            $documents[] = $this->unitOfWork->hydrate($rawDocument);
        }

        return $documents;
    }

    /**
     * @return Collection
     */
    public function getCollection()
    {
        return $this->collection;
    }

    public function remove(DocumentInterface $document)
    {
        return $this->collection->remove([
            '_id' => new \MongoId($document->getId())
        ]);
    }

    /**
     * @param Collection $collection
     * @return $this
     */
    public function setCollection($collection)
    {
        $this->collection = $collection;

        return $this;
    }

    /**
     * Finds objects by a set of criteria.
     *
     * Optionally sorting and limiting details can be passed. An implementation may throw
     * an UnexpectedValueException if certain values of the sorting or limiting details are
     * not supported.
     *
     * @param array      $criteria
     * @param array|null $orderBy
     * @param int|null   $limit
     * @param int|null   $offset
     *
     * @return DocumentInterface[] The objects.
     *
     * @throws \UnexpectedValueException
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        if (isset($criteria[0]) || empty($criteria)) {
            //This is aggregation query or query for all
        } else {

            if(isset($this->metadata->collection['typed'])) {
                if($this->metadata->collection['typed']) {
                    $criteria['__document_class'] = $this->metadata->collection['type'];
                }
            }

            $criteria = [
                [
                    '$match' => $criteria,
                ]
            ];
        }

        if (!is_null($orderBy)) {
            $criteria[] = [
                '$sort' => $orderBy,
            ];
        }

        if (!is_null($offset)) {
            $criteria[] = [
                '$skip' => $offset,
            ];
        }

        if (!is_null($limit)) {
            $criteria[] = [
                '$limit' => $limit,
            ];
        }
        $result = $this->collection->aggregate($criteria);

        $documents = [];

        foreach ($result as $rawDocument) {
            $documents[] = $this->unitOfWork->hydrate($rawDocument);
        }

        return $documents;
    }

    /**
     * Returns the class name of the object managed by the repository.
     *
     * @return string
     */
    public function getClassName()
    {
        return $this->metadata->getName();
    }

    /**
     * @return DocumentManager
     */
    public function getObjectManager()
    {
        return $this->documentManager;
    }

    /**
     * Finds a single object by a set of criteria.
     *
     * @param array $criteria The criteria.
     *
     * @return DocumentInterface The object.
     */
    public function findOneBy(array $criteria)
    {
        if(isset($this->metadata->collection['typed'])) {
            if($this->metadata->collection['typed']) {
                $criteria['__document_class'] = $this->metadata->collection['type'];
            }
        }
        $result = $this->collection->findOne($criteria);

        if ($result) {
            return $this->unitOfWork->hydrate($result);
        } else {
            return null;
        }
    }
}