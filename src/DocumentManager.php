<?php
/**
 * User: krona
 * Date: 9/29/14
 * Time: 6:17 PM
 */

namespace Krona\MongoODM;

use Doctrine\MongoDB\Database;
use Krona\Common\Common\ObjectHydrator;
use Krona\Common\ObjectManager;
use Krona\MongoODM\Mapping\ClassMetadata;
use Krona\MongoODM\Repository\DocumentRepository;
use Krona\MongoODM\Service\MetadataFactory;
use Krona\MongoODM\Service\RepositoryFactory;
use Zend\Stdlib\Hydrator\HydratorInterface;

class DocumentManager implements ObjectManager
{
    /** @var  RepositoryFactory */
    protected $repositoryFactory;
    /** @var  Database */
    protected $database;
    /** @var  array */
    protected $config;
    /** @var MetadataFactory */
    protected $metadataFactory;
    /** @var  ObjectHydrator */
    protected $objectHydrator;

    public function __construct(Database $database, array $config)
    {
        $this->database = $database;
        $this->repositoryFactory = new RepositoryFactory($this);
        $this->config = $config;
        $this->metadataFactory = new MetadataFactory($this->config['krona']);
    }

    /**
     * Finds an object by its identifier.
     *
     * This is just a convenient shortcut for getRepository($className)->find($id).
     *
     * @param string $className The class name of the object to find.
     * @param mixed  $id The identity of the object to find.
     *
     * @param        $identifier
     * @return object The found object.
     */
    public function find($className, $id, $identifier = '')
    {
        return $this->getRepository($className, $identifier)->find($id);
    }

    /**
     * Gets the repository for a class.
     *
     * @param string $className
     *
     * @param string $identifier
     * @return DocumentRepository
     */
    public function getRepository($className, $identifier = '')
    {
        return $this->repositoryFactory->getRepository($className, $identifier);
    }

    /**
     * @return Database
     */
    public function getDatabase()
    {
        return $this->database;
    }

    /**
     * @param Database $database
     * @return $this
     */
    public function setDatabase($database)
    {
        $this->database = $database;
        return $this;
    }

    /**
     * Tells the ObjectManager to make an instance managed and persistent.
     *
     * The object will be entered into the database as a result of the flush operation.
     *
     * NOTE: The persist operation always considers objects that are not yet known to
     * this ObjectManager as NEW. Do not pass detached objects to the persist operation.
     *
     * @param object $object The instance to make managed and persistent.
     *
     * @return void
     */
    public function persist($object)
    {
        // TODO: Implement persist() method.
    }

    /**
     * Removes an object instance.
     *
     * A removed object will be removed from the database as a result of the flush operation.
     *
     * @param object $object The object instance to remove.
     *
     * @return void
     */
    public function remove($object)
    {
        // TODO: Implement remove() method.
    }

    /**
     * Merges the state of a detached object into the persistence context
     * of this ObjectManager and returns the managed copy of the object.
     * The object passed to merge will not become associated/managed with this ObjectManager.
     *
     * @param object $object
     *
     * @return object
     */
    public function merge($object)
    {
        // TODO: Implement merge() method.
    }

    /**
     * Clears the ObjectManager. All objects that are currently managed
     * by this ObjectManager become detached.
     *
     * @param string|null $objectName if given, only objects of this type will get detached.
     *
     * @return void
     */
    public function clear($objectName = null)
    {
        // TODO: Implement clear() method.
    }

    /**
     * Detaches an object from the ObjectManager, causing a managed object to
     * become detached. Unflushed changes made to the object if any
     * (including removal of the object), will not be synchronized to the database.
     * Objects which previously referenced the detached object will continue to
     * reference it.
     *
     * @param object $object The object to detach.
     *
     * @return void
     */
    public function detach($object)
    {
        // TODO: Implement detach() method.
    }

    /**
     * Refreshes the persistent state of an object from the database,
     * overriding any local changes that have not yet been persisted.
     *
     * @param object $object The object to refresh.
     *
     * @return void
     */
    public function refresh($object)
    {
        // TODO: Implement refresh() method.
    }

    /**
     * Flushes all changes to objects that have been queued up to now to the database.
     * This effectively synchronizes the in-memory state of managed objects with the
     * database.
     *
     * @return void
     */
    public function flush()
    {
        // TODO: Implement flush() method.
    }

    /**
     * Returns the ClassMetadata descriptor for a class.
     *
     * The class name must be the fully-qualified class name without a leading backslash
     * (as it is returned by get_class($obj)).
     *
     * @param string $className
     *
     * @return ClassMetadata
     */
    public function getClassMetadata($className)
    {
        return $this->metadataFactory->getMetadataFor($className);
    }

    /**
     * Gets the metadata factory used to gather the metadata of classes.
     *
     * @return MetadataFactory
     */
    public function getMetadataFactory()
    {
        return $this->metadataFactory;
    }

    /**
     * Helper method to initialize a lazy loading proxy or persistent collection.
     *
     * This method is a no-op for other objects.
     *
     * @param object $obj
     *
     * @return void
     */
    public function initializeObject($obj)
    {
        // TODO: Implement initializeObject() method.
    }

    /**
     * Checks if the object is part of the current UnitOfWork and therefore managed.
     *
     * @param object $object
     *
     * @return bool
     */
    public function contains($object)
    {
        // TODO: Implement contains() method.
    }

    /**
     * @return HydratorInterface
     */
    public function getObjectHydrator()
    {
        if (is_null($this->objectHydrator)) {
            $this->objectHydrator = new ObjectHydrator($this, $this->config);
        }

        return $this->objectHydrator;
    }
}