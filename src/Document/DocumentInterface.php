<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 9/29/14
 * Time: 4:44 PM
 */

namespace Krona\MongoODM\Document;


use Krona\Common\Object\ObjectInterface;

interface DocumentInterface extends ObjectInterface
{
} 